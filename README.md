# Para aprender o shell do GNU/Linux

Coletânea de cursos, vídeos, artigos e livros que eu venho produzindo desde 2019.

## Para apoiar o meu trabalho

![](https://blauaraujo.com/wp-content/uploads/2022/05/cafezinho-01.png)

* [Apoio mensal pelo Apoia.se](https://apoia.se/debxpcursos)
* [Doações pelo PicPay](https://app.picpay.com/user/blauaraujo)
* Doações via PIX: pix@blauaraujo.com

## Vídeos

- [Intensivão de programação do Bash (59 vídeos)](https://youtube.com/playlist?list=PLXoSGejyuQGr53w4IzUzbPCqR4HPOHjAI)
- [Curso básico de programação do Bash (19 vídeos)](https://youtube.com/playlist?list=PLXoSGejyuQGpf4X-NdGjvSlEFZhn2f2H7)
- [Curso Shell GNU (19 vídeos)](https://youtube.com/playlist?list=PLXoSGejyuQGqJEEyo2fY3SA-QCKlF2rxO)
- [Sextas Shell (8 vídeos)](https://youtube.com/playlist?list=PLXoSGejyuQGouK99nsHs-4CUjr21QTAuy)
- [Criação de scripts em Bash (15 vídeos)](https://youtube.com/playlist?list=PLXoSGejyuQGrjEIS_tIJ7XYJTcc1ggQy-)

## Livros e cursos completos

- [Curso Básico de Programação em Bash](https://codeberg.org/blau_araujo/prog-bash-basico)
- [Curso básico de programação em AWK](https://codeberg.org/blau_araujo/curso-awk)
- [Curso Shell GNU](https://codeberg.org/blau_araujo/o-shell-gnu)
- [Curso de casamento de padrões no shell do GNU/Linux (PDF)](https://blauaraujo.com/baixar/)
- [Guia de estudos do curso intensivo de programação do Bash](https://blauaraujo.com/baixar/)
- [Livro: Pequeno Manual do Programador GNU/Bash (PDF)](https://blauaraujo.com/downloads/pmpgb.pdf)
- [Livro: Pequeno Manual do Programador GNU/Bash (EPUB)](https://blauaraujo.com/downloads/pmpgb.epub)

## Artigos

- [Repositório dos meus artigos](https://codeberg.org/blau_araujo/meus-artigos) (sempre em atualização)
